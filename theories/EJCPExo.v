(**

EJCP: hands-on session.

Author: Arthur Charguéraud.
License: MIT.

*)

Set Implicit Arguments.
Generalizable Variables A.
Require Import Example.
Require ExampleStack ExampleList.

Implicit Types n m : int.
Implicit Types p q : loc.



(* ####################################################### *)
(** * Basic functions *)

Module ExoBasic.

(** Hints: 
    - [xwp] to begin the proof
    - [xapp] for applications, or [xappn] to repeat
    - [xif] for a case analysis
    - [xval] for a value
    - [xsimpl] to prove entailments
    - [auto], [math], [rew_list] to prove pure facts
      or just [*] after a tactic to invoke automation.
*)


(* ******************************************************* *)
(** ** Basic pure function (warm up) *)

(** 
[[
  let double n =
    n + n
]]
*)

Definition double :=
  VFun 'n :=
    'n '+ 'n.

Lemma Triple_double : forall n,
  TRIPLE (double n)
    PRE \[]
    POST (fun m =>  (* FILL *) ).
Proof using.
   (* FILL *) 
Qed.


(* ******************************************************* *)
(** ** Basic imperative function with one argument *)

(** 
[[
  let inplace_double p =
    p := !p + !p
]]
*)

Definition inplace_double :=
  VFun 'p :=
    'p ':= ('!'p '+ '!'p).

Lemma Triple_inplace_double : forall p n,
  TRIPLE (inplace_double p)
    PRE ( (* FILL *) )
    POST (fun (_:unit) =>  (* FILL *) ).
Proof using.
   (* FILL *) 
Qed.


(* ******************************************************* *)
(** ** Basic imperative function with two arguments (white belt) *)

(** 
[[
  let decr_and_incr p q =
    decr p;
    incr q
]]
*)

Definition decr_and_incr :=
  VFun 'p 'q :=
    decr 'p ';
    incr 'q.

Lemma Triple_decr_and_incr : forall p q n m,
  TRIPLE (decr_and_incr p q)
    PRE ( (* FILL *) )
    POST ( (* FILL *) ).
Proof using.
   (* FILL *) 
Qed.


(* ******************************************************* *)
(** *** A recursive function (yellow belt) *) 

(** Here, we will assume [!p > 0].

[[
  let rec transfer p q =
    if !p > 0 then (
      decr p;
      incr q;
      transfer p q
    )
]]
*)

Definition transfer :=
  VFix 'f 'p 'q :=
    If_ '! 'p '> 0 Then
      decr 'p ';
      incr 'q ';
      'f 'p 'q
    End.

Lemma Triple_transfer : forall p q n m,
  n >= 0 ->
  TRIPLE (transfer p q)
    PRE (p ~~> n \* q ~~> m)
    POST (fun (_:unit) =>  (* FILL *) ).
Proof using.
  introv N. gen m N. induction_wf IH: (downto 0) n. intros.
   (* FILL *) 
Qed.

End ExoBasic.


(* ####################################################### *)
(** * Mutable lists *)

(** Hints: 
    - [xchange MList_eq] and the variants like for [Stackn]
    - [xchange (MList_not_nil p)] to unfold [p ~> MList L] when [L <> nil]
    - [xchange MList_cons] to unfold [p ~> MList (x::L)] 
      into [\exists q, p ~~> Cons x q \* q ~> MList L]
    - [xchange <- MList_cons] to fold back to [p ~> MList (x::L)]
    - [xchange <- (MList_cons p)], like the above for a specific [p]
    - [xchanges] is convenient here too.
*)

Module ExoList.
Import ExampleList.MList.


(* ******************************************************* *)
(** ** Create one element *)

(**
[[
  let mk_one x =
    mk_cons x (create())
]]
*)

Definition mk_one : val :=
  VFun 'x :=
     mk_cons 'x (create '()).

Lemma Triple_mk_one : forall A `{EA:Enc A} (x:A),
  TRIPLE (mk_one ``x)
    PRE \[]
    POST (fun p => p ~> MList (x::nil)).
Proof using.
   (* FILL *) 
Qed.

Hint Extern 1 (Register_Spec (mk_one)) => Provide @Triple_mk_one.


(* ******************************************************* *)
(** ** Push back using append *)

(** Note: [L&x] is a notation for [L++x::nil]. *)

(**
[[
  let push_back p x =
    inplace_append p (mk_one x)
]]
*)

(** Recall:
[[
  TRIPLE (inplace_append p1 p2)
    PRE (p1 ~> MList L1 \* p2 ~> MList L2)
    POST (fun (_:unit) => p1 ~> MList (L1++L2)).
]]
*)

Definition push_back : val :=
  VFun 'p 'x :=
    inplace_append 'p (mk_one 'x).

Lemma Triple_push_back : forall `{EA:Enc A} (L:list A) (x:A) (p:loc),
  TRIPLE (push_back ``p ``x)
    PRE (p ~> MList L)
    POST (fun (_:unit) => p ~> MList (L++x::nil)).
Proof using.
   (* FILL *) 
Qed.


(* ******************************************************* *)
(** ** Push back not using append (blue belt) *)

(** Hint: the following function is a specialization of 
    [inplace_append] for the case where the second list
    consists of a single element. Its proof is similar. *)

(**
[[
  let rec push_back' p x =
    if is_empty p 
      then set_cons p x (create())
      else push_back' (tail p) x
]]
*)

Definition push_back' : val :=
  VFix 'f 'p 'x :=
    If_ is_empty 'p 
      Then set_cons 'p 'x (create '())
      Else 'f (tail 'p) 'x.

Lemma Triple_push_back' : forall `{EA:Enc A} (L:list A) (x:A) (p:loc),
  TRIPLE (push_back' ``p ``x)
    PRE (p ~> MList L)
    POST (fun (_:unit) => p ~> MList (L++x::nil)).
Proof using.
  intros. gen p. induction_wf IH: (@list_sub A) L. intros.
   (* FILL *) 
Qed.


(* ******************************************************* *)
(** ** Reversed copy using iter (brown belt) *)

(** Hints: 
    - [xfun] to substitute a function definition in its occurences
    - [xapp (>> __ E)] to provide [E] as argument to the specification
      lemma that [xapp] would apply.
    - The proof has a similar pattern to [length_using_iter].
*)

(**
[[
  let rec reversed_copy p =
    let q = create() in
    iter (fun x => push q x) p;
    q
]]
*)

Definition reversed_copy : val :=
  VFun 'p :=
    Let 'q := create '() in
    iter (Fun 'x := push 'q 'x) 'p ';
    'q.

Lemma Triple_reversed_copy : forall A `{EA:Enc A} (L:list A) (p:loc),
  TRIPLE (reversed_copy ``p)
    PRE (p ~> MList L)
    POST (fun q => p ~> MList L \* q ~> MList (rev L)).
Proof using.
   (* FILL *) 
Qed.

End ExoList.


